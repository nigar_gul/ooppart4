public abstract class Room {
    private int roomNumber;
    private int dailyNight;
    private boolean available;
    private boolean booked;
    private int dailyRate;


    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    public int getRoomNumber() {
        return roomNumber;
    }

    public void setRoomNumber(int roomNumber) {
        this.roomNumber = roomNumber;
    }

    public double getDailyNight() {
        return dailyNight;
    }

    public void setDailyNight(double dailyNight) {
        this.dailyNight = (int) dailyNight;
    }

    public boolean isBooked() {
        return booked;
    }

    public void setBooked(boolean booked) {
        this.booked = booked;
    }

    public int getDailyRate() {
        return dailyRate;
    }

    public void setDailyRate(int dailyRate) {
        this.dailyRate = dailyRate;
    }

    public void checkingAvailability() {
        if (available) {
            available = false;
            System.out.println("Room is available");
        } else {
            System.out.println("Room is not available");
        }
    }

    public void booking() {
        if (booked) {
            booked = false;
            System.out.println("Room has booked");
        } else {
            System.out.println("Room  has not been booked");
        }
    }

    public abstract double calculatingCharges(int dailyNight);
}