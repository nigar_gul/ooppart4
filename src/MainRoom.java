public class MainRoom {
    public static void main(String[] args) {
        StandardRoom standardRoom=new StandardRoom();
        standardRoom.setRoomNumber(101);
        standardRoom.setDailyNight(5);
        standardRoom.setDailyRate(170);
        standardRoom.setAvailable(true);
        standardRoom.setBooked(true);
        standardRoom.checkingAvailability();
        standardRoom.calculatingCharges(5);
        standardRoom.booking();
        DeluxeRoom deluxeRoom=new DeluxeRoom();
        deluxeRoom.setRoomNumber(502);
        deluxeRoom.setNumberOfBeds(3);
        deluxeRoom.setDailyNight(6);
        deluxeRoom.setDailyRate(250);
        deluxeRoom.setAvailable(false);
        deluxeRoom.setBooked(false);
        deluxeRoom.checkingAvailability();
        deluxeRoom.calculatingCharges(0);
        deluxeRoom.booking();
        SuiteRoom suiteRoom= new SuiteRoom();
        suiteRoom.setRoomNumber(804);
        suiteRoom.setDailyNight(7);
        suiteRoom.setDailyRate(500);
        suiteRoom.livingRoom();
        suiteRoom.setAvailable(true);
        suiteRoom.setBooked(true);
        suiteRoom.livingRoom(true);
        suiteRoom.checkingAvailability();
        suiteRoom.calculatingCharges(7);
        suiteRoom.booking();
    }
}
