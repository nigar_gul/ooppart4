public class StandardRoom extends Room {


    public void checkingAvailability() {
        System.out.println("Standard room:");
        System.out.println("Room's number is " +getRoomNumber()+".");
        System.out.println("Total night is " +getDailyNight());
        System.out.println("Daily rate is " +getDailyRate()+ " AZN.");
        super.checkingAvailability();
    }

    public void booking() {
        super.booking();
    }

    @Override
    public double calculatingCharges(int dailyNight) {
        double total =getDailyRate() * getDailyNight();
        System.out.println("Total rate is: "+total+ " AZN.");
        return getDailyRate() * getDailyNight();
    }
}

