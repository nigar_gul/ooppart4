public class DeluxeRoom extends Room {

    private int numberOfBeds;

    public int getNumberOfBeds() {
        return numberOfBeds;
    }

    public void setNumberOfBeds(int numberOfBeds) {
        this.numberOfBeds = numberOfBeds;
    }

    public void checkingAvailability() {
        System.out.println("Deluxe room:");
        System.out.println("Room's number is " +getRoomNumber()+ ".");
        System.out.println("Number of beds is " +getNumberOfBeds()+".");
        System.out.println("Total night is " +getDailyNight());
        System.out.println("Daily rate is " +getDailyRate() + " AZN.");
        super.checkingAvailability();
    }

    public void booking() {
        super.booking();
    }

    @Override
    public double calculatingCharges(int dailyNight) {
        double total =getDailyRate() * getDailyNight();
        System.out.println("Total rate is: "+total+ " AZN.");
        return getDailyRate() * getDailyNight();
    }
}